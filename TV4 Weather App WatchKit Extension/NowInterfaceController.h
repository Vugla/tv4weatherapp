//
//  InterfaceController.h
//  TV4 Vädret WatchKit Extension
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface NowInterfaceController : WKInterfaceController

@end
