//
//  WeatherTableRowController.h
//  TV4 Weather App
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface WeatherTableRowController : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *hourLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *weatherImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *rainLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *windLabel;

@end
