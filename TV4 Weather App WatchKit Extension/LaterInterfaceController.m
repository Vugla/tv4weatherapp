//
//  LaterInterfaceController.m
//  TV4 Weather App
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#define APPID_OPENWEATHER @"c52041a4a92c3874394c361ca295356c"

#import "LaterInterfaceController.h"
#import "AFHTTPRequestOperationManager.h"
#import "WeatherTableRowController.h"

@interface LaterInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *locationLabel;
@property NSMutableArray* laterTodayForecasts;
@end

@implementation LaterInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    
    if ([defaults objectForKey:@"city"]) {
        [self.locationLabel setText:[defaults objectForKey:@"city"]];
    }else{
        [self.locationLabel setText:@"Unknown Location"];
    }

    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"lat" : [defaults objectForKey:@"lat"],
                             @"lon" : [defaults objectForKey:@"lon"],
                             @"units"    : @"metric",
                             @"APPID"      : APPID_OPENWEATHER};
    
    [manager GET:@"http://api.openweathermap.org/data/2.5/forecast" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSArray* list = [responseObject objectForKey:@"list"];
        self.laterTodayForecasts = [[NSMutableArray alloc]init];
        
        //taking only 24h forecast (at 3h intervals)
        for (int i=0; i<[list count]&&i<10;i++) {
            [self.laterTodayForecasts addObject:[list objectAtIndex:i ]];
           
        }
        
        [self.table setNumberOfRows:[self.laterTodayForecasts count] withRowType:@"WeatherRow"];
        for (int i =0; i<[self.laterTodayForecasts count]; i++) {
            WeatherTableRowController* row = [self.table rowControllerAtIndex:i];
            
            NSDictionary* tmp = [self.laterTodayForecasts objectAtIndex:i];
            NSDictionary* main = [tmp objectForKey:@"main"];
            NSDictionary* rain = [tmp objectForKey:@"rain"];
//            NSDictionary* wind = [tmp objectForKey:@"wind"];
            
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[tmp objectForKey:@"dt" ]intValue]];
          
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:NSCalendarUnitHour fromDate:date];
 
            if (components.hour<10) {
                [row.hourLabel setText:[NSString stringWithFormat:@"0%ld h",(long)components.hour]];
            }else{
            [row.hourLabel setText:[NSString stringWithFormat:@"%ld h",(long)components.hour]];
            }
            
            if ([main objectForKey:@"temp"]) {
                [row.temperatureLabel setText:[NSString stringWithFormat:@"%.0f˚",[[main objectForKey:@"temp"] doubleValue]]];
            }else{
                [row.temperatureLabel setText:@"--˚"];
            }
            
            if ([rain objectForKey:@"3h"] && [[NSString stringWithFormat:@"%.1f",[[rain objectForKey:@"3h"]doubleValue] ]doubleValue]>0) {
                [row.rainLabel setText:[NSString stringWithFormat:@"%.1f mm/h",[[rain objectForKey:@"3h"] doubleValue]]];
            }else{
                [row.rainLabel setText:@"0 mm/h"];
            }
            
//            if ([wind objectForKey:@"speed"] && [[NSString stringWithFormat:@"%.1f",[[wind objectForKey:@"speed"]doubleValue] ]doubleValue]>0) {
//                [row.windLabel setText:[NSString stringWithFormat:@"%.1f",[[wind objectForKey:@"speed"] doubleValue]]];
//            }else{
//                [row.windLabel setText:@"0"];
//            }
            
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



