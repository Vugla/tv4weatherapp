//
//  TommorowInterfaceController.m
//  TV4 Weather App
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#define APPID_OPENWEATHER @"c52041a4a92c3874394c361ca295356c"

#import "TommorowInterfaceController.h"
#import "AFHTTPRequestOperationManager.h"

@interface TommorowInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *locationLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *weatherImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lowHighTemperatureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *rainLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *windLabel;

@end

@implementation TommorowInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    [self populateInterface];
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"lat" : [defaults objectForKey:@"lat"],
                             @"lon" : [defaults objectForKey:@"lon"],
                             @"units"    : @"metric",
                             @"APPID"      : APPID_OPENWEATHER,
                             @"cnt":@"3"};
    
    [manager GET:@"http://api.openweathermap.org/data/2.5/forecast/daily" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSArray* list = [responseObject objectForKey:@"list"];
        NSInteger i=0;
        NSDictionary* tommorow;
        NSDate* now = [NSDate date];
        
        while (i<[list count]) {
            tommorow = [list objectAtIndex:i];
            
            if ([[tommorow objectForKey:@"dt"]integerValue]>[now timeIntervalSince1970]) {
                i = [list count];
            }else{
                tommorow = nil;
                i++;
            }

            
        }
        
        if (tommorow) {
            
            NSDictionary* temp = [tommorow objectForKey:@"temp"];
            [defaults setObject:[temp objectForKey:@"day"] forKey:@"tommorowTemp"];
            [defaults setObject:[temp objectForKey:@"min"] forKey:@"tommorowMinTemp"];
            [defaults setObject:[temp objectForKey:@"max"] forKey:@"tommorowMaxTemp"];
            
            if ([tommorow objectForKey:@"speed"]) {
                [defaults setObject:[tommorow objectForKey:@"speed"] forKey:@"tommorowWind"];
                
            }else{
                [defaults setObject:@"0" forKey:@"tommorowWind"];
                
            }
            if ([tommorow objectForKey:@"rain"]) {
                [defaults setObject: [tommorow objectForKey:@"rain"]forKey:@"tommorowRain"] ;
                
            }else{
                [defaults setObject:@"0" forKey:@"tommorowRain"];
                
            }

            
            
            
            [self populateInterface];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

-(void)populateInterface{
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    
    if ([defaults objectForKey:@"city"]) {
        [self.locationLabel setText:[defaults objectForKey:@"city"]];
    }else{
        [self.locationLabel setText:@"Unknown Location"];
    }
    
    if ([defaults objectForKey:@"tommorowTemp"]) {
        [self.temperatureLabel setText:[NSString stringWithFormat:@"%.0f˚",[[defaults objectForKey:@"tommorowTemp"]doubleValue] ]];
        self.weatherImage.hidden = NO;
    }else{
        [self.temperatureLabel setText:@"--˚"];
        self.weatherImage.hidden = YES;
    }
    
    if ([defaults objectForKey:@"tommorowWind"]) {
        [self.windLabel setText:[NSString stringWithFormat:@"%.1f m/s",[[defaults objectForKey:@"tommorowWind"]doubleValue] ]];
    }else{
        [self.windLabel setText:@"0 m/s"];
    }
    
    if ([defaults objectForKey:@"tommorowRain"]) {
        [self.rainLabel setText:[NSString stringWithFormat:@"%.1f mm/h",[[defaults objectForKey:@"tommorowRain"]doubleValue] ]];
    }else{
        [self.rainLabel setText:@"0 mm/h"];
    }
    
    if ([defaults objectForKey:@"tommorowMinTemp"]&&[defaults objectForKey:@"tommorowMaxTemp"]) {
        [self.lowHighTemperatureLabel setText:[NSString stringWithFormat:@"Low:%.0f˚ High:%.0f˚", [[defaults objectForKey:@"tommorowMinTemp"]doubleValue], [[defaults objectForKey:@"tommorowMaxTemp"]doubleValue]]];
    }else{
        [self.lowHighTemperatureLabel setText:@"Low:--˚ High:--˚"];
    }
    
    
}


@end



