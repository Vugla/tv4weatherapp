//
//  InterfaceController.m
//  TV4 Vädret WatchKit Extension
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#define APPID_OPENWEATHER @"c52041a4a92c3874394c361ca295356c"

#import "NowInterfaceController.h"
#import "AFHTTPRequestOperationManager.h"

@interface NowInterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *locationLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *weatherImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *rainLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *windLabel;

@end


@implementation NowInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    //set starting values retrieved from NSUserDefaults which are shared with iOS app
    [self populateInterface];
    
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    //refresh data by calling api, location is passed by iOS app in NSUserDefaults
    [WKInterfaceController openParentApplication:@{@"request":@"updateLocation"} reply:^(NSDictionary *replyInfo, NSError *error){
    
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"lat" : [defaults objectForKey:@"lat"],
                             @"lon" : [defaults objectForKey:@"lon"],
                             @"units"    : @"metric",
                             @"APPID"      : APPID_OPENWEATHER};
    
    [manager GET:@"http://api.openweathermap.org/data/2.5/weather" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSDictionary* main = [responseObject objectForKey:@"main"];
        NSDictionary* wind = [responseObject objectForKey:@"wind"];
        NSDictionary* rain = [responseObject objectForKey:@"rain"];
        
        
        [defaults setObject:[main objectForKey:@"temp"] forKey:@"temp"];
        
        if ([wind objectForKey:@"speed"]) {
            [defaults setObject:[wind objectForKey:@"speed"] forKey:@"wind"];
            
        }else{
            [defaults setObject:@"0" forKey:@"wind"];
            
        }
        if ([rain objectForKey:@"3h"]) {
            [defaults setObject: [rain objectForKey:@"3h"]forKey:@"rain"] ;
            
        }else{
            [defaults setObject:@"0" forKey:@"rain"];
            
        }
        
        NSLog(@"City: %@\nTemp: %@\n Wind: %@\n Rain: %@", [responseObject objectForKey:@"name"],[defaults objectForKey:@"temp"],[wind objectForKey:@"speed"],[rain objectForKey:@"3h"]);
        
        [self populateInterface];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
        
    }];
    
    
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}


-(void)populateInterface{
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    
    if ([defaults objectForKey:@"city"]) {
        [self.locationLabel setText:[defaults objectForKey:@"city"]];
    }else{
        [self.locationLabel setText:@"Unknown Location"];
    }
    
    if ([defaults objectForKey:@"temp"]) {
         [self.temperatureLabel setText:[NSString stringWithFormat:@"%.0f˚",[[defaults objectForKey:@"temp"]doubleValue] ]];
        self.weatherImage.hidden = NO;
    }else{
        [self.temperatureLabel setText:@"--˚"];
        self.weatherImage.hidden = YES;
    }
   
    if ([defaults objectForKey:@"wind"]) {
        [self.windLabel setText:[NSString stringWithFormat:@"%.1f m/s",[[defaults objectForKey:@"wind"]doubleValue] ]];
    }else{
        [self.windLabel setText:@"0 m/s"];
    }
    
    if ([defaults objectForKey:@"rain"]) {
        [self.rainLabel setText:[NSString stringWithFormat:@"%.1f mm/h",[[defaults objectForKey:@"rain"]doubleValue] ]];
    }else{
        [self.rainLabel setText:@"0 mm/h"];
    }
    
}

@end



