//
//  AppDelegate.h
//  TV4 Weather App
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

