//
//  AppDelegate.m
//  TV4 Weather App
//
//  Created by Predrag Samardzic on 18/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "AppDelegate.h"
#import "AFHTTPRequestOperationManager.h"

#define APPID_OPENWEATHER @"c52041a4a92c3874394c361ca295356c"
#define API_KEY_TIME @"AIzaSyCCF-UAuVMRCsijhNRqJTiLHtqXGvW2PjQ"

@interface AppDelegate ()

@end

@implementation AppDelegate
CLLocationManager *locationManager;

bool locationUpdated;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    

    //initializing location manager and starting updates
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    
    
    return YES;
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There was an error retrieving your location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [errorAlert show];
    NSLog(@"Error: %@",error.description);
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [locationManager stopUpdatingLocation];
    
    //retrieving location and saving coordinates in NSUserDefaults shared between iOS app and extension
    CLLocation* location = [locations lastObject];
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    [defaults setObject:[NSString stringWithFormat:@"%f", location.coordinate.longitude ] forKey:@"lon"];
    [defaults setObject:[NSString stringWithFormat:@"%f", location.coordinate.latitude  ] forKey:@"lat"];
    locationUpdated = YES;
    [self retrieveData];

    
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
//        NSLog(@"Finding address");
//        if (error) {
//            NSLog(@"Error %@", error.description);
//        } else {
//            CLPlacemark *placemark = [placemarks lastObject];
//                        [defaults setObject:placemark.locality forKey:@"city"];
//            [defaults setObject:placemark.country forKey:@"country"];
//                   }
//    }];
    
    
    
    //    latitude.text = [NSString stringWithFormat:@"%.8f",crnLoc.coordinate.latitude];
//    longitude.text = [NSString stringWithFormat:@"%.8f",crnLoc.coordinate.longitude];
   
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void (^)(NSDictionary *))reply{
    //execute this code in background queue and reply after location gets updated
    __block UIBackgroundTaskIdentifier watchKitHandler;
    watchKitHandler = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"backgroundTask"
                                                                   expirationHandler:^{
                                                                       watchKitHandler = UIBackgroundTaskInvalid;
                                                                   }];
    
    if ( [[userInfo objectForKey:@"request"] isEqualToString:@"updateLocation"] )
    {
        if (!locationUpdated) {
            [locationManager startUpdatingLocation];
        }
        //cycle until location gets updated before replying to WatchKit extension
        while (!locationUpdated) {
            
        }
        reply( @{@"status":@1} );
    }
    
    dispatch_after( dispatch_time( DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1 ), dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 ), ^{
        [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
    } );
}

- (void) retrieveData{
    
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.tv4weatherapp"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"lat" : [defaults objectForKey:@"lat"],
                             @"lon" : [defaults objectForKey:@"lon"],
                             @"units"    : @"metric",
                             @"APPID"      : APPID_OPENWEATHER};

    [manager GET:@"http://api.openweathermap.org/data/2.5/weather" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
           
            
            [defaults setObject:[responseObject objectForKey:@"name"] forKey:@"city"];
            
            NSDictionary* main = [responseObject objectForKey:@"main"];
            NSDictionary* wind = [responseObject objectForKey:@"wind"];
            NSDictionary* rain = [responseObject objectForKey:@"rain"];
            
            
            [defaults setObject:[main objectForKey:@"temp"] forKey:@"temp"];
            
            if ([wind objectForKey:@"speed"]) {
                [defaults setObject:[wind objectForKey:@"speed"] forKey:@"wind"];
                
            }else{
                [defaults setObject:@"0" forKey:@"wind"];
                
            }
            if ([rain objectForKey:@"3h"]) {
                [defaults setObject: [rain objectForKey:@"3h"]forKey:@"rain"] ;
                
            }else{
                [defaults setObject:@"0" forKey:@"rain"];
                
            }

            NSLog(@"City: %@\nTemp: %@\n Wind: %@\n Rain: %@", [responseObject objectForKey:@"name"],[defaults objectForKey:@"temp"],[wind objectForKey:@"speed"],[rain objectForKey:@"3h"]);
            
            

            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    
}
@end
